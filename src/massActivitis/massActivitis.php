<?php

class massActivitis {

    public $id = '';
    public $uniqueId = '';
    public $memberId = '';
    public $breakfastMeal = '';
    public $breakfastGuest = '';
    public $lunchMeal = '';
    public $lunchGuest = '';
    public $dinnerMeal = '';
    public $dinnerGuest = '';
    public $datetime = '';
    public $totalMeal = '';
    public $totalGuestMeal = '';
    public $dbConnect = '';
    public $dbUser = 'root';
    public $dbpassword = '';
    public $error = '';
    public $dbAllMeal = '';
    public $dbAllGroupData = '';
    public $shoppingDisription = '';
    public $shoppingTk = '';
    public $dbsingleMeal = '';
    public $dbsingleShopping = '';
    public $dbAllShopping = '';
    public $dbSearchMeal = '';
    public $month = '';
    public $year = '';

    public function __construct() {
        if (empty(session_id())) {
            session_start();
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->dbConnect = new PDO("mysql:host=localhost; dbname=massmanagement", $this->dbUser, $this->dbpassword) or die("Unable to Connect Database");
    }

    //constract


    public function prepare($formAllData='') {
        if (array_key_exists('id', $formAllData) && !empty($formAllData['id'])) {
            $this->id = $formAllData['id'];
        }

        if (array_key_exists('uniqueId', $formAllData) && !empty($formAllData['uniqueId'])) {
            $this->uniqueId = $formAllData['uniqueId'];
        }

        if (array_key_exists('memberId', $formAllData) && !empty($formAllData['memberId'])) {
            $this->memberId = $formAllData['memberId'];
        }

        if (array_key_exists('breakfastMeal', $formAllData) && !empty($formAllData['breakfastMeal'])) {
            $this->breakfastMeal = $formAllData['breakfastMeal'];
        } else {
            $this->breakfastMeal = "0";
        }

        if (array_key_exists('breakfastGuest', $formAllData) && !empty($formAllData['breakfastGuest'])) {
            $this->breakfastGuest = $formAllData['breakfastGuest'];
        }

        if (array_key_exists('lunchMeal', $formAllData) && !empty($formAllData['lunchMeal'])) {
            $this->lunchMeal = $formAllData['lunchMeal'];
        } else {
            $this->lunchMeal = "0";
        }

        if (array_key_exists('lunchGuest', $formAllData) && !empty($formAllData['lunchGuest'])) {
            $this->lunchGuest = $formAllData['lunchGuest'];
        }

        if (array_key_exists('dinnerMeal', $formAllData) && !empty($formAllData['dinnerMeal'])) {
            $this->dinnerMeal = $formAllData['dinnerMeal'];
        } else {
            $this->dinnerMeal = "0";
        }

        if (array_key_exists('dinnerGuest', $formAllData) && !empty($formAllData['dinnerGuest'])) {
            $this->dinnerGuest = $formAllData['dinnerGuest'];
        }

        if (array_key_exists('datetime', $formAllData) && !empty($formAllData['datetime'])) {
            $this->datetime = $formAllData['datetime'];
        }

        if (array_key_exists('shoppingDisription', $formAllData) && !empty($formAllData['shoppingDisription'])) {
            $this->shoppingDisription = $formAllData['shoppingDisription'];
        }

        if (array_key_exists('shoppingTk', $formAllData) && !empty($formAllData['shoppingTk'])) {
            $this->shoppingTk = $formAllData['shoppingTk'];
        }

        if (array_key_exists('month', $formAllData) && !empty($formAllData['month'])) {
            $this->month = $formAllData['month'];
        }

        if (array_key_exists('year', $formAllData) && !empty($formAllData['year'])) {
            $this->year = $formAllData['year'];
        }

//        echo '<pre>';
//        print_r($formAllData);
        $_SESSION['formData'] = $formAllData;
    }

    //end prepare method
    //
    //
    //
    //
    //
    //
    public function msgEcho($msg='') {
        if (isset($_SESSION["$msg"]) && !empty($_SESSION["$msg"])) {
            echo $_SESSION["$msg"];
            unset($_SESSION["$msg"]);
        }
    }

    //end sesstion echo Methode
    //
    //
    //
    //
    //
    //
    
    
    //******************************************************************************
    //start meal related all validation
    //******************************************************************************
    public function mealValidation() {
        $memberId = "'$this->memberId'";
        $datetime = "'$this->datetime'";
        $selectQuery = "SELECT * FROM `mealentry` WHERE `userId`=" . "$memberId" . " AND `mealDate`=" . "$datetime";

        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        $memberdata = $stmt->fetch(PDO::FETCH_ASSOC);
        //echo '<pre>';
        //print_r($memberdata);
        if (!empty($memberdata)) {
//            if ($this->memberId == $memberdata['userId']) {
//                $this->error = FALSE;
//            } else {
            $_SESSION['repeatdateErr'] = "This Date Meal already Added. You can just Edit from Member Mass Activities";
            $_SESSION['requiredimg'] = "Something Missing in your input";
            $this->error = TRUE;
            //}
        }
//        echo '<pre>';
//        print_r($memberdata);
//        die();
        if (empty($this->memberId)) {
            $_SESSION['memberIdErr'] = "You Must Select Mass Member";
            $_SESSION['requiredimg'] = "Something Missing in your input";
            $this->error = TRUE;
        }

        if ($this->breakfastGuest < 0) {
            $_SESSION['brekfastErr'] = "Nagetive Number are Not Allow";
            $_SESSION['requiredimg'] = "Something Missing in your input";
            $this->error = TRUE;
        }
        if ($this->lunchGuest < 0) {
            $_SESSION['lunchErr'] = "Nagetive Number are Not Allow";
            $_SESSION['requiredimg'] = "Something Missing in your input";
            $this->error = TRUE;
        }
        if ($this->dinnerGuest < 0) {
            $_SESSION['dinnerErr'] = "Nagetive Number are Not Allow";
            $_SESSION['requiredimg'] = "Something Missing in your input";
            $this->error = TRUE;
        }

        if (empty($this->datetime)) {
            $_SESSION['dateErr'] = "Date Required";
            $_SESSION['requiredimg'] = "Something Missing in your input";
            $this->error = TRUE;
        }
    }

    //end sesstion echo Methode
    //******************************************************************************
    // end meal related all Validation
    //******************************************************************************
    //
    //
    //
    //
    //
    //
    //
    //******************************************************************************
    //start shopping related all validation
    //******************************************************************************
    public function shoppingValidation() {
        $memberId = "'$this->memberId'";
        $datetime = "'$this->datetime'";
        $selectQuery = "SELECT * FROM `shoppingentry` WHERE `userId`=" . "$memberId" . " AND `shoppingDate`=" . "$datetime";
        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        $shoppingdata = $stmt->fetch(PDO::FETCH_ASSOC);
        //echo '<pre>';
        //print_r($memberdata);
        if (!empty($shoppingdata)) {
            $_SESSION['repeatdateErrr'] = "This Date Shopping already Added. You can just Edit from Member Mass Activities";
            $this->error = TRUE;
            //}
        }

        if (empty($this->memberId)) {
            $_SESSION['memberErr'] = "You Must Select Mass Member";
            $this->error = TRUE;
        }
        if (empty($this->shoppingDisription)) {
            $_SESSION['shopdisErrr'] = "Shopping Discription Required";
            $this->error = TRUE;
        }
        if (empty($this->shoppingTk)) {
            $_SESSION['shopTKErrr'] = "Shopping Taka Required";
            $this->error = TRUE;
        }

        if (empty($this->datetime)) {
            $_SESSION['dateErrr'] = "Date Required";
            $this->error = TRUE;
        }
    }

    //******************************************************************************
    //end shopping related all validation
    //******************************************************************************
    //
    //
    //
    //
    //
    //******************************************************************************
    //meal related all method
    //******************************************************************************

    public function addMeal() {
        if ($this->error == FALSE) {
            $this->totalMeal = $this->breakfastMeal + $this->lunchMeal + $this->dinnerMeal;
            $this->totalGuestMeal = $this->breakfastGuest + $this->lunchGuest + $this->dinnerGuest;
            try {
                $insertQuery = "INSERT INTO `mealentry` (`id`, `userId`, `mealDate`, `breakfastMeal`, `breakfastGuest`, `lunchMeal`, `lunchGuest`, `dinnerMeal`, `dinnerGuest`, `totalMeal`, `totalGuestMeal`) 
                    VALUES ( 'NULL', '$this->memberId', '$this->datetime', '$this->breakfastMeal', '$this->breakfastGuest', '$this->lunchMeal', '$this->lunchGuest', '$this->dinnerMeal', '$this->dinnerGuest', '$this->totalMeal', '$this->totalGuestMeal')";

                $stmt = $this->dbConnect->prepare($insertQuery);
                $stmt->execute();
                $_SESSION['mealAddSuccessMSg'] = "A Meal Successfully Added";
                header("location:mealEntry.php");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header('location:mealEntry.php');
        }
    }

    //end add meal Methode
    //
    //
    //
    //
    //
    //
    public function showAllMeal() {
        try {
            $starDate = date('Y-m-01');
            $endDate = date('Y-m-t');
            $selectQuery = "SELECT * FROM `mealentry` WHERE `mealDate` BETWEEN '$starDate' AND '$endDate' ORDER BY `mealDate` ASC ";
            $stmt = $this->dbConnect->prepare($selectQuery);
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->dbAllMeal[] = $row;
            }
            return $this->dbAllMeal;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    //end show all meal Methode
    //
    //
    //
    //
    //
    //
    

//    public function testSearch() {
//        try {
//            $starDate = $_POST['year'] . '-' . $_POST['month'] . '-' . date('01');
//            $endDate = $_POST['year'] . '-' . $_POST['month'] . '-' . date('t');
//
//            $selectQuery = "SELECT * FROM `mealentry` WHERE `mealDate` BETWEEN '$starDate' AND '$endDate' ORDER BY `mealDate` ASC ";
//            $stmt = $this->dbConnect->prepare($selectQuery);
//            $stmt->execute();
//            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
//                $this->dbSearchMeal[] = $row;
//            }
//            return $this->dbSearchMeal;
//        } catch (PDOException $e) {
//            echo 'Error: ' . $e->getMessage();
//        }
//    }
    //end search Methode
    //
    //
    //
    //
    //
    //

    public function showSingleMemberMeal() {
        $uniqueid = "'$this->uniqueId'";
        $selectQuery = "SELECT * FROM `memberresistration` LEFT JOIN `mealentry` ON `memberresistration`.`id`=`mealentry`.`userId` WHERE `memberresistration`.`uniqueId`=" . $uniqueid . "ORDER BY `mealDate` ASC";
        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->dbsingleMeal[] = $row;
        }
        return $this->dbsingleMeal;
    }

    //
    //
    //
    //
    //
    //end single member meal show method

    public function updateMeal() {
        if ($this->error == FALSE) {
            $id = "'$this->id'";
            $this->totalMeal = $this->breakfastMeal + $this->lunchMeal + $this->dinnerMeal;
            $this->totalGuestMeal = $this->breakfastGuest + $this->lunchGuest + $this->dinnerGuest;
            try {
                $date = date("Y-m-d h:i:s");
                $updateQuery = "UPDATE `mealentry` SET `mealDate` = '$this->datetime', `breakfastMeal` = '$this->breakfastMeal', `breakfastGuest` = '$this->breakfastGuest', `lunchMeal` = '$this->lunchMeal', `lunchGuest` = '$this->lunchGuest', `dinnerMeal` = '$this->dinnerMeal', `dinnerGuest` = '$this->dinnerGuest', `totalMeal` = '$this->totalMeal', `totalGuestMeal` = '$this->totalGuestMeal', `updated`= '$date' WHERE `mealentry`.`id` = " . $id;
                $stmt = $this->dbConnect->prepare($updateQuery);
                $stmt->execute();
                $_SESSION['mealUpdateSuccessMSg'] = "Meal Successfully Updated";
                header("location:singleMassActivities.php?uniqueId=$this->uniqueId");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("location:singleMassActivities.php?uniqueId=$this->uniqueId");
        }
    }

    //end add meal Methode
    //
    //
    //
    //
    //
    //
    //******************************************************************************
    //end meal related all method
    //******************************************************************************
    //
    //
    //
    //
    //
    //
    //******************************************************************************
    //start shopping related all method
    //******************************************************************************
    public function addShopping() {
        if ($this->error == FALSE) {
            try {
                $insertQuery = "INSERT INTO `shoppingEntry` (`id`, `userId`, `shoppingDate`, `shoppingDiscription`, `totalTk`, `creaded`) 
                    VALUES ( 'NULL', '$this->memberId', '$this->datetime', '$this->shoppingDisription', '$this->shoppingTk', :created)";

                $stmt = $this->dbConnect->prepare($insertQuery);
                $stmt->execute(array(
                    ':created' => date("Y-m-d h:i:s"),
                ));
                $_SESSION['mealAddSuccessMSg'] = "Your Shopping Successfully Added";
                header("location:shoppingEntry.php");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header('location:shoppingEntry.php');
        }
    }

    //end add shopping Methode
    //
    //
    //
    //
    //
    //
    public function showAllShopping() {
        $starDate = date('Y-m-01');
        $endDate = date('Y-m-t');
        //$selectQuery = "SELECT * FROM `shoppingentry` WHERE `shoppingDate` BETWEEN '$starDate' AND '$endDate' ORDER BY `shoppingDate` ASC ";
        //$selectQuery = "SELECT * FROM `shoppingentry` ORDER BY `shoppingDate` ASC";
        $selectQuery = "SELECT * FROM `shoppingentry` LEFT JOIN `memberresistration` ON `shoppingentry`.`userId`=`memberresistration`.`id` WHERE `shoppingDate` BETWEEN '$starDate' AND '$endDate' ORDER BY `shoppingDate` ASC";
        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->dbAllShopping[] = $row;
        }
        return $this->dbAllShopping;
    }

    //
    //
    //
    //
    //
    //end all shopping show method


    public function showSingleMemberShopping() {
        $uniqueid = "'$this->uniqueId'";
        $selectQuery = "SELECT * FROM `memberresistration` LEFT JOIN `shoppingentry` ON `memberresistration`.`id`=`shoppingentry`.`userId` WHERE `memberresistration`.`uniqueId`=" . $uniqueid . "ORDER BY `shoppingDate` ASC";
        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->dbsingleShopping[] = $row;
        }
        return $this->dbsingleShopping;
    }

    //
    //
    //
    //
    //
    //end single member shopping show method

    public function updateShopping() {
        if ($this->error == FALSE) {
            $id = "'$this->id'";
            try {
                $date = date("Y-m-d h:i:s");
                $updateQuery = "UPDATE `shoppingEntry` SET `shoppingDate` = '$this->datetime', `shoppingDiscription` = '$this->shoppingDisription', `totalTk` = '$this->shoppingTk', `updated`= '$date' WHERE `shoppingEntry`.`id` = " . $id;
                $stmt = $this->dbConnect->prepare($updateQuery);
                $stmt->execute();
                $_SESSION['shoppingUpdateSuccessMSg'] = "Shopping Successfully Updated";
                header("location:singleMassActivities.php?uniqueId=$this->uniqueId");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("location:singleMassActivities.php?uniqueId=$this->uniqueId");
        }
    }

    //end add shopping Methode
    //
    //
    //
    //
    //
    //
    //******************************************************************************
    //end shopping related all method
    //******************************************************************************
}

//end class
?>
