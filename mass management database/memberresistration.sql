-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2017 at 09:40 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `massmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `memberresistration`
--

CREATE TABLE `memberresistration` (
  `id` int(11) NOT NULL,
  `uniqueId` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `fatherName` varchar(255) NOT NULL,
  `education` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `nationalid` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `isAdmin` int(11) NOT NULL,
  `isActive` int(11) NOT NULL,
  `isDelete` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `memberresistration`
--

INSERT INTO `memberresistration` (`id`, `uniqueId`, `name`, `fatherName`, `education`, `email`, `password`, `phone`, `nationalid`, `address`, `image`, `date`, `isAdmin`, `isActive`, `isDelete`, `created`, `updated`, `deleted`) VALUES
(3, '57f9beb316061', 'new member', 'member er bap', 'amar education from diu', 'ase@email.com', '1234567', 2147483647, '523697', 'a:3:{i:0;s:5:"dhaka";i:1;s:12:"district nai";i:2;s:3:"234";}', '58028eb697997 bombardment-clipart-angry_man.png', '08 Oct 2016', 0, 1, 0, '2016-10-09 09:51:15', '2016-10-16 02:17:31', '2016-11-10 03:16:07'),
(4, '57f9c0af0918ef', 'new member', 'member r bapd', 'amar ducatiodn from diu', 'tarek@tarek.com', '123456', 214783647, '5236', 'a:3:{i:0;s:5:"dhaka";i:1;s:12:"district nai";i:2;s:3:"234";', '57fbdb289c028 1513326_478023705641698_1241367057_n.jpg', '08 Oct 207', 0, 1, 0, '2016-10-09 09:59:43', '2016-10-09 00:00:00', '2016-10-22 02:32:13'),
(5, '57f9c0e93175d', 'sakib all hasan', 'father', 'amar education from diu', 'abubakar.belal@yahoo.com', '123456', 2147483647, 'ghsesr', 'a:3:{i:0;s:5:"dhaka";i:1;s:12:"district nai";i:2;s:3:"234";}', '57f9c0e93175d 536837_708378739187566_458211135_n.jpg', '08 Oct 2016', 1, 1, 0, '2016-10-09 10:00:41', '0000-00-00 00:00:00', '2016-11-10 03:15:36'),
(6, '57fab3809e8c5', 'abu bakar belal', 'Md. Helal Uddin Moyshan', 'BSc in CSE from Dhaka International University', 'abu@belal.com', 'belal12', 1916952769, '199163524789536', 'a:3:{i:0;s:8:"Asrarpur";i:1;s:9:"Narsingdi";i:2;s:4:"1620";}', '57fab3809e4dd Belal  (86).jpg', '09 Oct 2016', 0, 1, 0, '2016-10-10 03:15:44', '0000-00-00 00:00:00', '2016-11-10 03:15:33'),
(7, '57fbdb493f72a', 'aowlad Hossain', 'Md. Raju Moyshan', 'BSc in CSE from Dhaka International University', 'rakib@rakib.com', 'rakib123', 1912666450, '1230456789', 'a:3:{i:0;s:8:"Asrarpur";i:1;s:9:"Narsingdi";i:2;s:4:"1620";}', '57fbdb493f342 1514581_538452982967107_4454890492470497295_n.jpg', '10 Oct 2016', 0, 1, 0, '2016-10-11 12:17:45', '0000-00-00 00:00:00', '2016-11-10 03:15:29'),
(8, '58031493d417b', 'shabuj ahmed', 'mamun mia', 'BSc in CSE from DIU', 'sabuj@vi.com', '123456', 2147483647, '210364', 'a:3:{i:0;s:5:"dhaka";i:1;s:5:"dhaka";i:2;s:4:"1230";}', '58031493d39ab 1526608_1505580856332738_488415078_n.jpg', '15 Oct 2016', 0, 1, 0, '2016-10-16 11:48:03', '0000-00-00 00:00:00', '2016-11-10 03:15:25'),
(9, '58049b1593948', 'sorif', 'nai', 'under metrik', 'sorif@mia.com', '123456', 2147483647, '1246', 'a:3:{i:0;s:5:"dhaka";i:1;s:12:"district nai";i:2;s:4:"1209";}', '58049b158bc47 2015-men-pompadour-a-guide-to-the-modern-pompadour-hairstyle-Picture.jpg', '17 Oct 2016', 0, 1, 0, '2016-10-17 03:34:13', '0000-00-00 00:00:00', '2016-11-10 03:15:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `memberresistration`
--
ALTER TABLE `memberresistration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `memberresistration`
--
ALTER TABLE `memberresistration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
