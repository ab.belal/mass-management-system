<?php
include_once '../src/resistrationLogin/resistrationLogin.php';
$objResistration = new resistrationLogin();
$allMemberData = $objResistration->showAllMassMember();

include_once '../src/massActivitis/massActivitis.php';
$objMassActivitis = new massActivitis();
//$allMeal = $objMassActivitis->showAllMeal();
$dballShopping = $objMassActivitis->showAllShopping();

//*********************
//single member
//*********************
//foreach ($allMemberData as $singleMember) {
//    //echo $singleMember['id'];
//}
////*********************
////total meal
////*********************
//$totalBreakFastMeal = $totalLunchMeal = $totalDinnerMeal = $totalGuestMeal = 0;
//foreach ($allMeal as $mMeal) {
//    $totalBreakFastMeal+=$mMeal['breakfastMeal'];
//    $totalLunchMeal+=$mMeal['lunchMeal'];
//    $totalDinnerMeal+=$mMeal['dinnerMeal'];
//    $totalGuestMeal+=$mMeal['totalGuestMeal'];
//}
//$TotalMeal = $totalBreakFastMeal + $totalLunchMeal + $totalDinnerMeal + $totalGuestMeal;
//print_r($_SESSION['loginedUser']);
if (isset($_SESSION['loginedUser']) && !empty($_SESSION['loginedUser'])) {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Shopping Activities</title>
            <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>

            <!-- google font CDN -->
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

            <!--bootstrap CDN-->
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

            <!-- Optional theme -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

            <!-- Global stylesheets -->
            <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
            <!-- /global stylesheets -->


            <!-- my all custom css file-->
            <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="css/memberAddFromStyle.css" rel="stylesheet" type="text/css">
            <link href="css/mealEntryFormDesign.css" rel="stylesheet" type="text/css">
            <link href="css/wholeMassActivites.style.css" rel="stylesheet" type="text/css">

        </head>

        <body>
            <!-- Main navbar -->
            <div class="navbar navbar-inverse custom-style">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html"><img src="assets/images/app-loog.png" alt="app-logo"></a>

                    <ul class="nav navbar-nav visible-xs-block">
                        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                        <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>

                </div>

                <div class="navbar-collapse collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav">
                        <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown dropdown-user">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <img height="46px" width="46px" src="images/massMemberImage/<?php echo $_SESSION['loginedUser']['image'] ?>" alt="user image">
                                <span><?php echo ucfirst($_SESSION['loginedUser']['name']); ?><br/></span>
                                <i class="caret"></i><br/>
                                <span class="admin">
                                    <?php
                                    if ($_SESSION['loginedUser']['isAdmin'] == 1) {
                                        echo "Admin";
                                    } else {
                                        echo "User";
                                    }
                                    ?>
                                </span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="singleMemberView.php?uniqueId=<?php echo $_SESSION['loginedUser']['uniqueId'] ?>"><i class="icon-user"></i> My profile</a></li>
                                <li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /main navbar -->


            <!-- Page container -->
            <div class="page-container">

                <!-- Page content -->
                <div class="page-content">

                    <!-- Main sidebar -->
                    <div class="sidebar sidebar-main">
                        <div class="sidebar-content">

                            <!-- User menu -->
                            <div class="sidebar-user">
                                <div class="category-content">
                                    <div class="media">
                                        <a href="#" class="media-left"><img src="images/massMemberImage/<?php echo $_SESSION['loginedUser']['image'] ?>" class="img-circle img-sm" alt=""></a>
                                        <div class="media-body">
                                            <span class="media-heading text-semibold"><?php echo ucfirst($_SESSION['loginedUser']['name']); ?></span>
                                            <div class="text-size-mini text-muted">
                                                <i class="icon-pin text-size-small"></i> &nbsp;L-12, Kazi Najrul Islam Road, Mohammadpur-1207
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /user menu -->


                            <!-- Main navigation -->
                            <div class="sidebar-category sidebar-category-visible">
                                <div class="category-content no-padding">
                                    <ul class="navigation navigation-main navigation-accordion">
                                        <!-- Main -->
                                        <li><a href="allMassMembers.php"><i class="icon-users4"></i> <span>Mass Members</span></a></li>
                                        <?php if ($_SESSION['loginedUser']['isAdmin'] == 1) { ?>
                                            <li><a href="memberAdd.php"><i class="icon-user-plus"></i> <span>Add Mass Members</span></a></li>
                                        <?php } ?>
                                        <li><a href="mealEntry.php"><i class="icon-droplets"></i> <span>Add Meal</span></a></li>
                                        <li><a href="shoppingEntry.php"><i class="icon-basket"></i> <span>Add Shopping</span></a></li>
                                        <li class="active">
                                            <a href="#" class="has-ul"><i class="icon-tree7"></i> <span>Whole Mass Activities</span></a>
                                            <ul>
                                                <li><a href="mealList.php"><i class="icon-stack2"></i> Meal Activities</a></li>
                                                <li class="active"><a href="shopping.php"><i class="icon-cart2"></i> Shopping Activities</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="finalReport.php"><i class="icon-briefcase3"></i> <span>Monthly Final Report</span></a></li>
                                        <li><a href="trashList.php"><i class="icon-blocked"></i> <span>Blocked Mass Members</span></a></li>
                                        <!-- /main -->
                                    </ul>
                                </div>
                            </div>
                            <!-- /main navigation -->
                        </div>
                        <!-- /sidebar-content -->
                    </div>
                    <!-- /main sidebar -->


                    <!-- Main content -->
                    <div class="content-wrapper">

                        <!-- Page header -->
                        <div class="page-header" style="margin-bottom: 0px;">
                            <div class="page-header-content">
                                <div class="page-title">
                                    <h4 class="custom-icon-size"><i class="icon-cart2 position-left"></i> <span class="text-semibold">Shopping Activities</span></h4>
                                </div>
                            </div>

                            <div class="breadcrumb-line">
                                <ul class="breadcrumb">
                                    <li><a href="allMassMembers.php"><i class="icon-users4 position-left"></i>All Mass Members</a></li>
                                    <li class="active">Shopping Activities</li>
                                </ul>
                            </div>
                        </div>
                        <!-- /page header -->

                        <style type="text/css">
                            .breadcrumb > li + li:before {
                                content: "\f101 ";
                                font-family: FontAwesome;
                            }                            
                        </style>

                        <!-- Content area -->
                        <div class="content">

                            <!-- Main charts -->
                            <div class="row">
                                <div class="col-lg-12 mealList">
                                    <div class="memberAdd panel panel-flat">
                                        <div class="panel-heading">
                                            <h2 class="table-header pull-left"><?php echo date('F - Y'); ?></h2>
                                            <div class="dataFilterSearch pull-right">
                                                <form action="" method="post" class="form-inline">
                                                    <div class="form-group">
                                                        <select name="month" class="form-control">
                                                            <option value="" >-- Select Month --</option>
                                                            <option value="01" >January</option>
                                                            <option value="02">February</option>
                                                            <option value="03">March</option>
                                                            <option value="04">April</option>
                                                            <option value="05">May</option>
                                                            <option value="06">Jun</option>
                                                            <option value="07">July</option>
                                                            <option value="08">August</option>
                                                            <option value="09">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="year" class="form-control">
                                                            <option value="">-- Select Year --</option>
                                                            <option value="2017">2017</option>
                                                            <option value="2016">2016</option>
                                                            <option value="2015">2015</option>
                                                            <option value="2014">2014</option>
                                                            <option value="2013">2013</option>
                                                            <option value="2012">2012</option>
                                                            <option value="2011">2011</option>
                                                            <option value="2010">2010</option>
                                                        </select>
                                                    </div>
                                                    <button name="search1" type="submit" class="btn btn-default">Search</button>
                                                </form>
                                                <!-- /search form -->
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /row -->

                            <div class="row">
                                <div id="mass-member-mesonary">
                                    <?php
                                    if (isset($_POST['search1'])) {

                                        $dbConn = new PDO("mysql:host=localhost; dbname=massmanagement", 'root', '') or die("Unable to Connect Database");

                                        $starDate = $_POST['year'] . '-' . $_POST['month'] . '-' . date('01');
                                        $endDate = $_POST['year'] . '-' . $_POST['month'] . '-' . date('t');

                                        $selectQuery = "SELECT * FROM `shoppingentry` WHERE `shoppingDate` BETWEEN '$starDate' AND '$endDate' ORDER BY `shoppingDate` ASC ";
                                        $stmt = $dbConn->prepare($selectQuery);
                                        $stmt->execute();

                                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                            $dbSearchShopping[] = $row;
                                            $allShopping = $dbSearchShopping;
                                        }
                                        if (empty($allShopping)) {
                                            echo"<h1 style='text-align:center'>Search Data Not Found...!</h1>";
                                        }
                                    } else {
                                        $allShopping = $dballShopping;
                                    }

                                    if (isset($allShopping) && !empty($allShopping)) {
                                        $shoppingUserId = array_column($allShopping, 'userId');
                                        $uniqueShoppingUserId = array_unique($shoppingUserId);

                                        foreach ($uniqueShoppingUserId as $singleUniqueShoppingUserId) {

                                            //for individual shopping cost count
                                            $totalShoppingCost = 0;
                                            foreach ($allShopping as $singleShopping) {
                                                if ($singleUniqueShoppingUserId == $singleShopping['userId']) {
                                                    $totalShoppingCost+=$singleShopping['totalTk'];
                                                }
                                            }
                                            ?>

                                            <div class="col-lg-4 allShopping">
                                                <div class="panel panel-flat">
                                                    <div class="panel-heading" style="padding: 0px;">
                                                        <div class="thumbnail" style="margin: 0px;">
                                                            <?php
                                                            foreach ($allMemberData as $oneMember) {
                                                                if ($singleUniqueShoppingUserId == $oneMember['id']) {
                                                                    ?>
                                                                    <div class="thumb pull-left">
                                                                        <img style="width: 100%; height: 100%;" src="images/massMemberImage/<?php echo $oneMember['image'] ?>" alt="member image">
                                                                    </div>
                                                                    <div class="thum-name pull-left">
                                                                        <h2><?php echo $oneMember['name']; ?></h2>
                                                                        <p>
                                                                            <span>shopping cost : <?php echo "<span style='color:red; font-weight:bold'>" . $totalShoppingCost . "</span>"; ?></span> 
                                                                        </p>
                                                                    </div>
                                                                <?php }
                                                            } ?>
                                                            <div style="clear: both;"></div>
                                                        </div>
                                                        <!-- /traffic sources -->
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered">
                                                                <thead> 
                                                                    <tr> 
                                                                        <th>Date</th>
                                                                        <th>Shopping Details</th>
                                                                        <th style="text-align: center;">Taka</th>
                                                                    </tr> 
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    foreach ($allShopping as $singleShopping) {
                                                                        if ($singleUniqueShoppingUserId == $singleShopping['userId']) {
                                                                            ?>
                                                                            <tr>
                                                                                <td><?php echo $singleShopping['shoppingDate']; ?></td>
                                                                                <td><?php echo $singleShopping['shoppingDiscription']; ?></td>
                                                                                <td style="text-align: center;"><?php echo $singleShopping['totalTk']; ?></td>
                                                                            </tr>
                                                                        <?php }
                                                                    } ?>
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr style="font-weight: bold; text-align: center;">
                                                                        <td colspan="2">Total Shopping : </td>
                                                                        <td style="text-align: center;"><?php echo $totalShoppingCost; ?></td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                        <!--/table responsive-->
                                                    </div>
                                                    <!--/pannel hedding-->
                                                </div>
                                                <!--/pannel-->
                                            </div>
                                            <!--/col-md-4-->
                                            <?php
                                        }
                                    } else {
                                        if (!isset($_POST['search1'])) {
                                            echo "<h1 style='text-align:center;'>Nobody Shopping Yet...!</h1>";
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <!-- /main charts -->




                            <!-- Footer -->
                            <div class="footer text-muted">
                                &copy; 2016. <a href="#">Mass Managment Web App </a> by <a href="http://abbelal.tk" target="_blank">AB Belal</a>
                            </div>
                            <!-- /footer -->

                        </div>
                        <!-- /content area -->

                    </div>
                    <!-- /main content -->

                </div>
                <!-- /page content -->

            </div>
            <!-- /page container -->

            <!-- Latest compiled and minified JavaScript CDN -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

            <!-- Core JS files -->
            <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/js/core/app.js"></script>

            <!-- datetime picker js -->
            <script type="text/javascript" src="js/moment.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>

            <!--masonary plugin for all member-->
            <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>


            <script type="text/javascript">
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })
                /* datetime picker activation */
                $(function() {
                    $('.datetimepicker1').datetimepicker({
                        useCurrent: true,
                        format: "DD-MMM-YYYY",
                        ignoreReadonly: true
                        //minDate: new Date()

                        /* if i want to select client only specify date then the code will beauty-carousel
                                 minDate: "2016-02-20",
                                 maxDate: "2016-02-25" */
                    });
                });


            </script>
            <script type="text/javascript">
                                                                                                                                                        
                /* masonry activation for porfolio */
                $( window ).load( function()
                {
                    $( '#mass-member-mesona ry' ).masonry({ 
                        itemSelector: '.allShopping' 
                    });
                                                                                                                        	
                                                                                                                        	
                });
            </script>

        </body>
    </html>
    <?php
} else {
    $_SESSION['pageErr'] = "You have to login first";
    header('location:login.php');
}
?>