<?php
include_once '../src/resistrationLogin/resistrationLogin.php';
$objResistration = new resistrationLogin();
//print_r($_SESSION['loginedUser']);
if (isset($_SESSION['loginedUser']) && !empty($_SESSION['loginedUser'])) {
    ?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Add Mass Member</title>
            <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>

            <!-- google font CDN -->
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

            <!--bootstrap CDN-->
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

            <!-- Optional theme -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

            <!-- Global stylesheets -->
            <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
            <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
            <!-- /global stylesheets -->


            <!-- my all custom css file-->
            <link href="css/memberAddFromStyle.css" rel="stylesheet" type="text/css">

        </head>

        <body>

            <!-- Main navbar -->
            <div class="navbar navbar-inverse custom-style">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html"><img src="assets/images/app-loog.png" alt="app-logo"></a>

                    <ul class="nav navbar-nav visible-xs-block">
                        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                        <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>
                </div>

                <div class="navbar-collapse collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav">
                        <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown dropdown-user">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <img height="46px" width="46px" src="images/massMemberImage/<?php echo $_SESSION['loginedUser']['image'] ?>" alt="user image">
                                <span><?php echo ucfirst($_SESSION['loginedUser']['name']); ?></span>
                                <i class="caret"></i><br/>
                                <span class="admin">
                                    <?php
                                    if ($_SESSION['loginedUser']['isAdmin'] == 1) {
                                        echo "Admin";
                                    } else {
                                        echo "User";
                                    }
                                    ?>
                                </span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#"><i class="icon-user"></i> My profile</a></li>
                                <li><a href="logout.php"><i class="icon-switch2"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /main navbar -->


            <!-- Page container -->
            <div class="page-container">

                <!-- Page content -->
                <div class="page-content">

                    <!-- Main sidebar -->
                    <div class="sidebar sidebar-main">
                        <div class="sidebar-content">

                            <!-- User menu -->
                            <div class="sidebar-user">
                                <div class="category-content">
                                    <div class="media">
                                        <a href="#" class="media-left"><img src="images/massMemberImage/<?php echo $_SESSION['loginedUser']['image'] ?>" class="img-circle img-sm" alt=""></a>
                                        <div class="media-body">
                                            <span class="media-heading text-semibold"><?php echo ucfirst($_SESSION['loginedUser']['name']); ?></span>
                                            <div class="text-size-mini text-muted">
                                                <i class="icon-pin text-size-small"></i> &nbsp;L-12, Kazi Najrul Islam Road, Mohammadpur-1207
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /user menu -->


                            <!-- Main navigation -->
                            <div class="sidebar-category sidebar-category-visible">
                                <div class="category-content no-padding">
                                    <ul class="navigation navigation-main navigation-accordion">
                                        <!-- Main -->
                                        <li><a href="allMassMembers.php"><i class="icon-users4"></i> <span>Mass Members</span></a></li>
                                        <?php if ($_SESSION['loginedUser']['isAdmin'] == 1) { ?>
                                            <li class="active"><a href="memberAdd.php"><i class="icon-user-plus"></i> <span>Add Mass Members</span></a></li>
                                        <?php } ?>
                                        <li><a href="mealEntry.php"><i class="icon-droplets"></i> <span>Add Meal</span></a></li>
                                        <li><a href="shoppingEntry.php"><i class="icon-basket"></i> <span>Add Shopping</span></a></li>
                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-tree7"></i> <span>Whole Mass Activities</span></a>
                                            <ul class="hidden-ul" style="display: none;">
                                                <li><a href="mealList.php"><i class="icon-stack2"></i> Meal Activities</a></li>
                                                <li><a href="shopping.php"><i class="icon-cart2"></i> Shopping Activities</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="finalReport.php"><i class="icon-briefcase3"></i> <span>Monthly Final Report</span></a></li>
                                        <li><a href="trashList.php"><i class="icon-blocked"></i> <span>Blocked Mass Members</span></a></li>
                                        <!-- /main -->
                                    </ul>
                                </div>
                            </div>
                            <!-- /main navigation -->
                        </div>
                        <!-- /sidebar-content -->
                    </div>
                    <!-- /main sidebar -->


                    <!-- Main content -->
                    <div class="content-wrapper">

                        <!-- Page header -->
                        <div class="page-header">
                            <div class="page-header-content">
                                <div class="page-title">
                                    <h4 class="custom-icon-size"><i class="icon-user-plus position-left"></i> <span class="text-semibold">Add New Mass Member</span></h4>
                                </div>
                            </div>

                            <div class="breadcrumb-line">
                                <ul class="breadcrumb">
                                    <li><a href="allMassMembers.php"><i class="icon-users4 position-left"></i> Mass Members</a></li>
                                    <li class="active">New Member Add</li>
                                </ul>
                            </div>
                        </div>
                        <!-- /page header -->


                        <!-- Content area -->
                        <div class="content">

                            <!-- Main charts -->
                            <div class="row">
                                <div class="col-lg-12">

                                    <!-- Traffic sources -->
                                    <div class="memberAdd panel panel-flat">
                                        <div class="panel-heading">

                                            <?php if (!empty($_SESSION['addSuccessMSg'])) { ?>
                                                <div class="alert alert-success alert-styled-left" style="margin: 0 10px 10px 10px; width: 45%; padding: 10px 14px;">
                                                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                                    <?php $objResistration->msgEcho('addSuccessMSg'); ?>
                                                </div>
                                            <?php } ?>

                                            <form action="memberAddProcess.php" method="POST" enctype="multipart/form-data">
                                                <div class="form-group fullName">
                                                    <label>Full name <span style="color: red;">*</span></label>
                                                    <input name="fullName" class="form-control" type="text" value="<?php
                                        if (isset($_SESSION['formData']['fullName'])) {
                                            echo $_SESSION['formData']['fullName'];
                                            unset($_SESSION['formData']['fullName']);
                                        }
                                            ?>">
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('nameErr'); ?></p>
                                                </div>
                                                <div class="form-group fatherName">
                                                    <label>Father's Name</label>
                                                    <input name="fatherName" class="form-control" type="text" value="<?php
                                                       if (isset($_SESSION['formData']['fatherName'])) {
                                                           echo $_SESSION['formData']['fatherName'];
                                                           unset($_SESSION['formData']['fatherName']);
                                                       }
                                            ?>"> 
                                                </div>

                                                <div class="form-group education">
                                                    <label>Education</label>
                                                    <input name="eduTitle" class="form-control" type="text" placeholder="ex : Education Title from Institute name" value="<?php
                                                       if (isset($_SESSION['formData']['eduTitle'])) {
                                                           echo $_SESSION['formData']['eduTitle'];
                                                           unset($_SESSION['formData']['eduTitle']);
                                                       }
                                            ?>"> 
                                                </div>
                                                <div style="clear:both;"></div>

                                                <div class="form-group email">
                                                    <label>Email <span style="color: red;">*</span></label>
                                                    <input name="email" class="form-control" type="email" value="<?php
                                                       if (isset($_SESSION['formData']['email'])) {
                                                           echo $_SESSION['formData']['email'];
                                                           unset($_SESSION['formData']['email']);
                                                       }
                                            ?>">
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('emailErr'); ?></p>
                                                </div>

                                                <div class="form-group password">
                                                    <label>Password <span style="color: red;">*</span></label>
                                                    <input name="password" class="form-control" type="password" value="<?php
                                                       if (isset($_SESSION['formData']['password'])) {
                                                           echo $_SESSION['formData']['password'];
                                                           unset($_SESSION['formData']['password']);
                                                       }
                                            ?>">
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('passwordErr'); ?></p>
                                                </div>

                                                <div class="form-group repassword">
                                                    <label>Confirm Password <span style="color: red;">*</span></label>
                                                    <input name="confirmPassword" class="form-control" type="password" value="<?php
                                                       if (isset($_SESSION['formData']['confirmPassword'])) {
                                                           echo $_SESSION['formData']['confirmPassword'];
                                                           unset($_SESSION['formData']['confirmPassword']);
                                                       }
                                            ?>">
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('repPasswordErr'); ?></p>
                                                </div>
                                                <div style="clear:both;"></div>

                                                <div class="form-group phone">
                                                    <label>Phone <span style="color: red;">*</span></label>
                                                    <input name="phone" class="form-control" type="number" value="<?php
                                                       if (isset($_SESSION['formData']['phone'])) {
                                                           echo $_SESSION['formData']['phone'];
                                                           unset($_SESSION['formData']['phone']);
                                                       }
                                            ?>">
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('phoneErr'); ?></p>
                                                </div>

                                                <div class="form-group nationalId">
                                                    <label>National ID <span style="color: red;">*</span></label>
                                                    <input name="nationalId" class="form-control" type="text" value="<?php
                                                       if (isset($_SESSION['formData']['nationalId'])) {
                                                           echo $_SESSION['formData']['nationalId'];
                                                           unset($_SESSION['formData']['nationalId']);
                                                       }
                                            ?>">
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('nidErr'); ?></p>
                                                </div>

                                                <div class="addingDate">
                                                    <label>Member Joining Date <span style="opacity:0;">*</span></label>
                                                    <div class="input-group date" id="datetimepicker1">
                                                        <input type="text" name="date" readonly="readonly" class="form-control" placeholder="Date"  value="<?php
                                                       if (isset($_SESSION['formData']['date'])) {
                                                           echo $_SESSION['formData']['date'];
                                                           unset($_SESSION['formData']['date']);
                                                       }
                                            ?>" />
                                                        <span class="input-group-addon">
                                                            <span style="cursor: pointer;" class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                    <p class="text-danger"></p>
                                                </div>
                                                <div style="clear:both;"></div>

                                                <div class="form-group address1">
                                                    <label>Address <span style="color: red;">*</span></label>
                                                    <input name="address" class="form-control" type="text" placeholder="ex : House no, Road name / Village" value="<?php
                                                           if (isset($_SESSION['formData']['address'])) {
                                                               echo $_SESSION['formData']['address'];
                                                               unset($_SESSION['formData']['address']);
                                                           }
                                            ?>">
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('addressErr'); ?></p>
                                                </div>

                                                <div class="form-group city">
                                                    <label>City/District <span style="color: red;">*</span></label>
                                                    <input name="city" class="form-control" type="text" value="<?php
                                                       if (isset($_SESSION['formData']['city'])) {
                                                           echo $_SESSION['formData']['city'];
                                                           unset($_SESSION['formData']['city']);
                                                       }
                                            ?>">
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('cityErr'); ?></p>
                                                </div>

                                                <div class="form-group zip">
                                                    <label>Zip Code <span style="color: red;">*</span></label>
                                                    <input name="zip" class="form-control" type="text" value="<?php
                                                       if (isset($_SESSION['formData']['zip'])) {
                                                           echo $_SESSION['formData']['zip'];
                                                           unset($_SESSION['formData']['zip']);
                                                       }
                                            ?>">
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('zipErr'); ?></p>
                                                </div>
                                                <div style="clear:both;"></div>

                                                <div class="form-group img">
                                                    <label style="display: inline; margin-right: 0px;">Upload image</label>
                                                    <input name="image" class="form-control" style="display: inline;" type="file" value="">
                                                    <p class="text-danger"> <?php $objResistration->msgEcho('imageErr'); ?></p>
                                                </div>
                                                <div style="clear:both;"></div>

                                                <div class="text-left" style="margin-top: 20px;">
                                                    <button type="submit" class="btn btn-primary submit">Add New Member <i class="icon-user-plus position-right"></i></button>                                                                                                                
                                                </div>
                                            </form>
                                        </div>                                    
                                    </div>
                                    <!-- /traffic sources -->

                                </div>
                            </div>
                            <!-- /main charts -->



                            <!-- Footer -->
                            <div class="footer text-muted">
                                &copy; 2016. <a href="#">Mass Managment Web App </a> by <a href="http://abbelal.tk" target="_blank">AB Belal</a>
                            </div>
                            <!-- /footer -->

                        </div>
                        <!-- /content area -->

                    </div>
                    <!-- /main content -->

                </div>
                <!-- /page content -->

            </div>
            <!-- /page container -->

            <!-- Latest compiled and minified JavaScript CDN -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

            <!-- Core JS files -->
            <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/js/core/app.js"></script>


            <!-- datetime picker js -->
            <script type="text/javascript" src="js/moment.js"></script>
            <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>

            <script type="text/javascript">
                /* datetime picker activation */
                $(function () {
                    $('#datetimepicker1').datetimepicker({
                        useCurrent:true,
                        format:"DD MMM YYYY",
                        ignoreReadonly:true
                        //minDate: new Date()
    			
                        /* if i want to select client only specify date then the code will beauty-carousel
                    minDate: "2016-02-20",
                    maxDate: "2016-02-25" */
                    });
                });
            </script>

        </body>
    </html>
    <?php
} else {
    $_SESSION['pageErr'] = "You have to login first";
    header('location:login.php');
}
?>